"""AIMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include, patterns
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^$','AIMS_Tz.views.index',name='index'),
    url(r'^books/page/(\d+)/?$', 'AIMS_Tz.views.index_library', name='index'),
    url(r'^books/?$', 'AIMS_Tz.views.index_library', name='index_library'),
    url(r'^index/?$', 'AIMS_Tz.views.index', name='index'),
    url(r'^archives/?$', 'AIMS_Tz.views.archive', name='archive'),
    url(r'^news/?$', 'AIMS_Tz.views.new', name='news'),
    url(r'^students/?$', 'AIMS_Tz.views.student', name='students'),
    url(r'^news/(\d+)/?$', 'AIMS_Tz.views.show_news', name='show_news'),
    url(r'^students/(\d+)/?$', 'AIMS_Tz.views.show_students', name='show_students'),
    url(r'^books/(\d+)/?$', 'AIMS_Tz.views.show', name='show'),
    url(r'^books/new/?$', 'AIMS_Tz.views.new', name='new'),
    url(r'^books/create/?$', 'AIMS_Tz.views.create', name='create'),
    url(r'^books/(\d+)/edit/?$', 'AIMS_Tz.views.edit', name='edit'),
    url(r'^books/(\d+)/update/?$', 'AIMS_Tz.views.update', name='update'),
    url(r'^books/(\d+)/destroy/?$', 'AIMS_Tz.views.destroy', name='destroy'),
    url(r'^books/(\d+)/checkout$', 'AIMS_Tz.views.checkout', name='checkout'),
    url(r'^admin/', include(admin.site.urls)),
]

if settings.DEBUG:
  urlpatterns.append(url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))