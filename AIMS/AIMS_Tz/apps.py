from __future__ import unicode_literals

from django.apps import AppConfig


class AimsTzConfig(AppConfig):
    name = 'AIMS_Tz'
