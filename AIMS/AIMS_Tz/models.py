from __future__ import unicode_literals

from django.db import models
from datetime import datetime

# Create your models here.
class News(models.Model):
    title = models.CharField(max_length=200)
    story = models.CharField(max_length=5000)
    docfile = models.ImageField(upload_to='top_news/%Y/%m/%d')
    author = models.CharField(max_length=50)
    published = models.DateField(default = datetime.now, editable= False)
    time = models.TimeField(default = datetime.now, editable= False)

    def __str__(self):
		return self.title

class Students(models.Model):
    title = models.CharField(max_length=200)
    story = models.CharField(max_length=5000)
    docfile = models.ImageField(upload_to='Students_news/%Y/%m/%d')
    author = models.CharField(max_length=50)
    published = models.DateField(default = datetime.now, editable= False)
    time = models.TimeField(default = datetime.now, editable= False)

    def __str__(self):
		return self.title

class Archives(models.Model):
    title = models.CharField(max_length=200)
    abstract = models.CharField(max_length=5000)
    docfile = models.FileField(upload_to='Essays/%Y/%m/%d')
    author = models.CharField(max_length=200)
    published = models.DateField(default = datetime.now, editable= False)
    time = models.TimeField(default = datetime.now, editable= False)


    def __str__(self):
		return self.title




class Book(models.Model):
    title = models.CharField(max_length=75)
    author = models.CharField(max_length=50)
    publisher = models.CharField(max_length=50)
    isbn = models.CharField(max_length=13, blank=False)
    checked_out = models.BooleanField(default=False)
    year = models.CharField(max_length=20)
    quantity = models.IntegerField(default=1)
    image_url = models.CharField(max_length=300, default="/static/img/default-book.png")
    summary = models.TextField(max_length=255, default="Enter a summary of the book here.")
    is_donated = models.BooleanField(default=False)
    checked_out_by = models.CharField(max_length=100, default="Nobody")

    def __str__(self):
        return "Title: %s" % self.title

    @staticmethod
    def _from_data(filename):
        data = DataImporter().read_file(filename)
        books = []
        for book in data:
            books.append(Book(title=book['title'],
                              author=book['author'],
                              publisher=book['publisher'],
                              year=book['year'],
                              quantity=book['quantity'],
                              is_donated=book['is_donated']
                              ))
        return books

    @staticmethod
    def store_all(filename):
        books = Book._from_data(filename)
        for book in books:
            book.save()
