from django.conf.urls import url,include, patterns
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('AIMS_Tz.views',
    url(r'^$','index', name='index'),
    url(r'^list/$', 'index', name='index'),
    url(r'^admin/', include(admin.site.urls)),
                       )
