from django import forms
from  django.forms import ModelForm
from AIMS_Tz.models import Book, News, Students, Archives


class DocumentForm(forms.Form):
   docfile = forms.FileField(
      label='Select a file',
 )

class NewsForm(ModelForm):
    class meta:
        model = News

        fields=['title', 'story', 'docfile', 'author', 'published','time']

class StudentsForm(ModelForm):
    class meta:
        model = Students

        fields=['title', 'story', 'docfile', 'author', 'published','time']


class ArchivesForm(ModelForm):
    class meta:
        model = Archives

        fields=['title', 'abstarct', 'docfile', 'author', 'published','time'
                                                                      '']

class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = ['title', 'author', 'publisher', 'isbn', 'year', 'quantity', 'image_url', 'checked_out', 'summary',
                  'is_donated', 'checked_out_by',]


