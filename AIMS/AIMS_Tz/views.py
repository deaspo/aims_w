from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage
from AIMS_Tz.models import Book, News, Students, Archives
from AIMS_Tz.forms import BookForm, DocumentForm

# Create your views here.

def index(request):
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = News(docfile = request.FILES['docfile'])
            newdoc.save()

            # Redirect to the document index page after POST
            return HttpResponseRedirect(reverse('index'))
    else:
        form = DocumentForm() # A empty, unbound form

    # Load documents for the index page
    # documents = News.objects.latest('time')
    documents = News.objects.last()
    # students = Students.objects.latest('time')
    students = Students.objects.last()

    # Render list page with the documents and the form
    return render_to_response('news/index.html',{'documents': documents,'students': students, 'form': form},context_instance=RequestContext(request))

def new(request):
    news = News.objects.all().order_by('time')
    return render_to_response('news/news.html',{'documents': news})

def student(request):
    students = Students.objects.all().order_by('time')
    return render_to_response('students/students.html',{'students': students})
#
def archive(request):
    archives = Archives.objects.all().order_by('time')
    return render_to_response('archives/archives.html',{'archives': archives})

def show_news(request, new_id):
    new = get_object_or_404(News, pk=new_id)
    return render(request, 'news/show_news.html', {'doc': new})

def show_students(request, student_id):
    student = get_object_or_404(Students, pk=student_id)
    return render(request, 'students/show_students.html', {'doc': student})


def _paginate_books(page):
    book_list = Book.objects.all()
    paginator = Paginator(book_list, 15)
    try:
        books = paginator.page(page)
    except EmptyPage:
        books = paginator.page(paginator.num_pages)
    return books, paginator


def index_library(request, page=1):
    books, paginator = _paginate_books(page)
    return render(request, "books/index.html", {"books": books, "paginator": paginator})


def show(request, book_id):
    book = Book.objects.get(id=book_id)
    return render(request, "books/show.html", {"book": book})


def new(request):
    form = BookForm()
    return render(request, "books/new.html", {"form": form})


def create(request):
    if not request.POST:
        return redirect("/books/new")

    form = BookForm(request.POST)

    if form.is_valid():
        book = form.save()
        return redirect("/books/%d" % book.id)
    else:
        return render(request, "books/new.html", {"form": form})


def edit(request, book_id):
    book = Book.objects.get(id=book_id)
    form = BookForm(instance=book)
    return render(request, "books/edit.html", {"form": form, "book": book})


def update(request, book_id):
    book = Book.objects.get(id=book_id)
    form = BookForm(request.POST, instance=book)

    if form.is_valid():
        book = form.save()
        return redirect("/books/%d" % book.id)
    else:
        return render(request, "books/edit.html", {"form": form, "book": book})


def destroy(request, book_id):
    Book.objects.get(id=book_id).delete()

    return redirect("/books")

def checkout(request, book_id):
    book = Book.objects.get(id=book_id)

    if not request.POST:
        form = BookForm(instance=book)
        return render(request, "books/checkout.html", {"form": form, "book": book})
    else:
        form = BookForm(request.POST, instance=book)
        form.save()
        books, paginator = _paginate_books(1)

        return redirect("/books/", {"books": books, "paginator": paginator})