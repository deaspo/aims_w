from django.contrib import admin
from  AIMS_Tz.models import Book, News, Students, Archives
admin.site.register(Book)
admin.site.register(News)
admin.site.register(Students)
admin.site.register(Archives)

# Register your models here.
